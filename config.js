const path = require('path');
const APP_LANG = process.env.APP_LANG ? process.env.APP_LANG : 'en';
const BOT_TOKEN = process.env.APP_BOT_TOKEN;

module.exports = {
	lang: APP_LANG,
	bot_token: BOT_TOKEN,
	langsFolder: path.resolve(__dirname, 'locales'),
	timezone: 'UTC',
	formatDate: 'YYYY-MM-DD HH:mm:ss',
	middleware: [],
	commands: {
		start: require('./handlers/start'),
		help: require('./handlers/help')
	},
	hears: {
		menu_posts: require('./handlers/users/index'),
		menu_posts_create: require('./handlers/users/create')
	},
	scenes: [],
	actions: [
		// [action(regex|string), handler(require module)]
	],
	paginator: {
		page: 1,
		per_page: 5,
		total: 0,
		data: []
	},
	scenarios: {
		models: {
			users: require('./models/users')
		},
		keyboards: {
			main: ({ i18n }) => {
				return [
					[i18n.t('menu_posts')],
					[i18n.t('menu_about')]
				];
			},
			users: ({ i18n }) => {
				return [
					[i18n.t('menu_posts_create')],
					[i18n.t('octoba_main')]
				];
			}
		}
	}
};
