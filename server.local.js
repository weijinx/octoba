console.log('Loading env vars...');
require('dotenv').config();

console.log('Starting local server...');
const app = require('./index');
app.initBot();
