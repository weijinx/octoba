const config = require('./config');
const App = require('octoba-io-framework');

module.exports = new App(config);
