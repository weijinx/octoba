module.exports = {
	id: 'users',
	name: 'Users',
	pk: 'id',
	crud: {
		query: {
			uri: 'users',
			label_attribute: 'first_name'
		},
		item: {
			uri: 'users',
			title_attribute: 'id',
			image_id: null,
			image_url: 'avatar',
			render_attributes: ['id', 'first_name'],
			render_update_attributes: ['first_name', 'last_name'],
			hears_handler: 'menu_posts'
		},
		store: {
			uri: 'users',
			form: {
				confirm_before_create: true,
				prepare_form_message: true,
				attributes: ['first_name', 'last_name'],
				validators: {
					first_name: 'text@min:3'
				}
			}
		},
		update: {
			uri: 'users',
			form: {
				prepare_form_message: false,
				attributes: ['first_name', 'last_name', 'avatar'],
				validators: {
					first_name: 'text@min:5|max:10',
					last_name: 'text@min:4',
					avatar: 'photo@'
				}
			}
		},
		update_one: {
			uri: 'users'
		},
		delete: {
			uri: 'users'
		}
	},
	labels: {
		id: 'ID',
		first_name: 'Firstname',
		last_name: 'Lastname',
		avatar: 'Avatar'
	}
};
