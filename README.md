# Octoba Application - easy way to create bots in Telegram.

Before we start I want to create my view of this framework. The main goal is to give for developers a perfect tool to create bots (crud included) for Telegram.
I'm not a pro with NodeJS, but I love Telegram and Telegram bots as well. So, let's dive into application.

## Requirements

To install and use this tool, you need to stick to these requirements:
* NodeJS: v10.3.0 or newer
* NPM: v6.5.0 or newer

We use [Octoba](https://github.com/octoba-io/framework) and [Telegraf](https://telegraf.js.org/) Frameworks as a sub-layers of our application.

1. You need to install package ``npm i --save octoba-io-app``
2. Read more in [documentation](https://github.com/octoba-io/octoba#documentation).

## Documentation

### Setup local environment
1. Copy ``.env-example`` to ``.env``
2. Open ``.env`` and paste bot token ``BOT_TOKEN=...`` which you can receive from [@BotFather](https://t.me/BotFather)
3. Run ``npm i`` to install all package dependencies.
4. Run ``npm start`` to start bot.

**That's it!**

### Localizations
Localizations for your application located in ``./locales`` folder.
If you need to add new localization you may easy do it by adding new locale configuration.
To do that you need to create a new ``{name}.yaml`` file where name is a locale.
To use some locale as default you need to edit key ``APP_LANG`` from ``en (default locale)`` to new one.

### Application configuration
Okay. Now we need to configure our application. In the root folder of the project you can see ``./config.js`` file.
This is the main config file of our feature bot. Let's dive into configuration!

#### ``./config.js`` API
* ``lang`` - language of application (default ``en``).
* ``bot_token`` - Telegram Bot Token.
* ``langsFolder`` - location of your locales. If you want to change directory of locales, you can config it here.
* ``timezone`` - application timezone.
* ``formatDate`` - default dates format.
* ``middleware`` - array of middlewares.
* ``commands`` - commands which bot can handle.
Default commands: ``start`` and ``help``.

* ``hears`` - hears (keyboard clicks) which bot can handle.
* ``scenes`` - scenes which bot can handle.
* ``actions`` - actions (inline keyboard clicks) which bot can handle.
* ``paginator`` - default pagination options.
``page`` - default loading page (``Default value: 1``)
``per_page`` - default items on page (``Default value: 5``)
* ``scenarios`` - has models and keyboards sections.
``models`` - object of ``{name: value}`` where name - name of model and value is model config.
``keyboards`` - object of ``{name: fn(app)}`` where name is a name of keyboard and ``fn(app)`` is a function to return keyboard settings.

## Want to help us?
If you have ideas to improve application or you have noticed some issues, let us know.
Of course, pull requests are extremely welcome!

## Contributors
1. [@weijinx](https://github.com/weijinnx)
