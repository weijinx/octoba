const Extra = require('telegraf/extra');

module.exports = ctx => {
	return ctx.reply(
		'About bot.',
		Extra.markdown().markup(m => {
			return m.keyboard(ctx.getKeyboard('main'));
		})
	);
};
