const Extra = require('telegraf/extra')

module.exports = async ctx => {
	ctx.replyWithMarkdown(
		ctx.match[0],
		Extra.markdown().markup(m => {
			return m.keyboard(ctx.getKeyboard('users'))
		})
	)

	let params = ctx.config.paginator

	return ctx.itemsWithPagination(ctx, 'users', params)
}
