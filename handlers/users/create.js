module.exports = ctx => {
	ctx.replyWithMarkdown(ctx.match[0]);
	const model = ctx.config.scenarios.models.users;

	return ctx.scene.enter('octoba-form-many', {
		form: {
			uri: model.crud.store.uri,
			validators: model.crud.store.form.validators,
			prepare: model.crud.store.form.prepare_form_message
				? model.crud.store.form.prepare_form_message
				: false,
			attributes: model.crud.store.form.attributes,
			labels: model.labels,
			type: 'store',
			pk_name: model.pk
		},
		data: {}
	});
};
