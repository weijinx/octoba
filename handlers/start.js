const Extra = require('telegraf/extra');

module.exports = ctx => {
	return ctx.reply(
		'Hello World!',
		Extra.markdown().markup(m => {
			return m.keyboard(ctx.getKeyboard('main'));
		})
	);
};
